/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.nickname;

import me.boomboompower.nickname.api.utils.MessageUtils;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Main class for the 'Nicknames' plugin
 *
 * @author boomboompower
 * @version 3.5
 */
public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        Status.ENABLE.log();
        new Registration(this);

        saveDefaultConfig();
    }

    @Override
    public void onDisable() {
        Status.DISABLE.log();
    }

    /**
     * Console logging system via enums
     */
    private enum Status {
        ENABLE("&aEnabled"),
        DISABLE("&cDisabled");

        private String message;

        /**
         * The message that the enum value contains
         *
         * @param message Message to be set to that enum
         */
        Status(String message) {
            this.message = message;
        }

        /**
         * Logs the enum message with it's color
         *          invokes the {@link MessageUtils#translate(String)} method
         */
        public void log() {
            MessageUtils.sendToConsole("&c- - - - - -&b Nicknames &c- - - - -");
            MessageUtils.sendToConsole("       &7Has been " + message + "&7!");
            MessageUtils.sendToConsole("&c- - - - - -&b Nicknames &c- - - - -");
        }
    }
}

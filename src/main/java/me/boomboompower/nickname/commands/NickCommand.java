/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.nickname.commands;

import me.boomboompower.nickname.Main;
import me.boomboompower.nickname.api.NicknameAPI;
import me.boomboompower.nickname.events.PlayerNicknameEvent;
import me.boomboompower.nickname.api.utils.Actionbar;
import me.boomboompower.nickname.api.utils.MessageUtils;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import org.spigotmc.SpigotConfig;

/**
 * Nickname command - Customizable messages.
 *
 * @author boomboompower
 * @version 3.5
 */
public class NickCommand implements CommandExecutor {

    private final String NONE = "";

    private final String NICK_ARGS_RELOAD = "reload";
    private final String NICK_ARGS_REMOVE = "remove";

    private final String NICK_PERMISSION = "nickname.nick";
    private final String NICK_PERMISSION_ADMIN = "nickname.admin";

    private final String NICK_NO_PERMISSION = SpigotConfig.unknownCommandMessage;

    private final String NICK_NO_NICKNAME = "&cYou do not currently have a nickname!";
    private final String NICK_IN_USE = "&cThis nickname is already being used!";
    private final String NICK_REMOVED = "&cYour nickname has been removed!";

    private final String NICK_RELOAD_SUCCESSFUL = "&cSuccessfully reloaded config!";
    private final String NICK_RELOAD_UNSUCCESSFUL1 = "&cAn error occured while reloading the config!";
    private final String NICK_RELOAD_UNSUCCESSFUL2 = "&cCheck for errors in the console!";

    private final String NICK_IS_NOW = "&cYour nickname is now: &4&l";
    private final String NICK_IS_NOW_EXCLAMATION = "&c!";

    private final String NICK_REQUIREMENTS = "&cYour nickname must consist of letters, numbers and not be an online players name!";
    private final String NICK_BAD_ARGUMENTS = "&cIncorrect usage. Use &4/nick <nickname>&c instead!";
    private final String NICK_DENY_CONSOLE = "&cOnly a player may execute this command!";

    private final String COMMAND = "nick";
    private final Main PLUGIN;

    /**
     * Creates a new instance of this class
     *
     * @param main Main class
     */
    public NickCommand(Main main) {
        this.PLUGIN = main;

        main.getCommand(COMMAND).setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase(COMMAND)) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (!player.hasPermission(NICK_PERMISSION.toLowerCase())) {
                    MessageUtils.sendToPlayer(player, NICK_NO_PERMISSION);
                } else {
                    boolean nick = true;
                    if (args.length > 0) {
                        if (args[0].equalsIgnoreCase(NICK_ARGS_REMOVE)) {
                            if (NicknameAPI.isNicked(player) && NicknameAPI.getNickname(player) != null) {
                                NicknameAPI.removeNickname(player);
                                Actionbar.sendActionbar(player, NONE);
                                MessageUtils.sendToPlayer(player, NICK_REMOVED);
                            } else {
                                MessageUtils.sendToPlayer(player, NICK_NO_NICKNAME);
                            }
                        } else if (args[0].equalsIgnoreCase(NICK_ARGS_RELOAD) && player.hasPermission(NICK_PERMISSION_ADMIN.toLowerCase())) {
                            try {
                                PLUGIN.reloadConfig();
                                MessageUtils.sendToPlayer(player, NICK_RELOAD_SUCCESSFUL);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                MessageUtils.sendToPlayer(player, NICK_RELOAD_UNSUCCESSFUL1);
                                MessageUtils.sendToPlayer(player, NICK_RELOAD_UNSUCCESSFUL2);
                            }
                        } else {
                            for (char car : args[0].toCharArray()) if (!Character.isLetterOrDigit(car)) nick = false;
                            for (Player online : Bukkit.getOnlinePlayers()) if (args[0].equals(online.getName())) nick = false;
                            if (nick) {
                                if (!NicknameAPI.getNickname(player).toString().equalsIgnoreCase(args[0]) && !NicknameAPI.isNickname(args[0])) {
                                    PlayerNicknameEvent event = new PlayerNicknameEvent(player, args[0]);
                                    Bukkit.getPluginManager().callEvent(event);
                                    if (!event.isCancelled()) {
                                        NicknameAPI.setNickname(player, event.getNickname());
                                        MessageUtils.sendToPlayer(player, NICK_IS_NOW + event.getNickname() + NICK_IS_NOW_EXCLAMATION);
                                    }
                                } else {
                                    MessageUtils.sendToPlayer(player, NICK_IN_USE);
                                }
                            } else {
                                MessageUtils.sendToPlayer(player, NICK_REQUIREMENTS);
                            }
                        }
                    } else {
                        MessageUtils.sendToPlayer(player, NICK_BAD_ARGUMENTS);
                    }
                }
            } else {
                MessageUtils.sendToConsole(NICK_DENY_CONSOLE);
            }
        }
        return true;
    }
}

/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.nickname.listeners;

import com.google.common.collect.Maps;
import me.boomboompower.nickname.Main;
import me.boomboompower.nickname.api.NicknameAPI;
import me.boomboompower.nickname.api.utils.Actionbar;
import me.boomboompower.nickname.api.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;

import static me.boomboompower.nickname.api.NicknameAPI.*;
import static me.boomboompower.nickname.api.Placeholders.getPlaceHolders;

public class Nicknames implements Listener {

    private final Main PLUGIN;
    private static HashMap<Player, CharSequence> nicknames = Maps.newHashMap();

    public Nicknames(Main main) {
        PLUGIN = main;

        Bukkit.getPluginManager().registerEvents(this, main);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(PLUGIN, new Runnable() {
            @Override
            public void run() {
                for (Player player : nicknames.keySet()) {
                    Actionbar.sendActionbar(player, "&fYour name is currently: &c&l" + getNickname(player) + "&f!");
                }
            }
        }, 0, 20);
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onPlayerChat(PlayerChatEvent event) {
        Player player = event.getPlayer();
        String format = string("format");
        format = format.replace("%NAME%", isNicked(player) ? getNickname(player) : player.getName()).replace("%MESSAGE%", event.getMessage());
        try {
            for (CharSequence key : getPlaceHolders().keySet()) {
                for (CharSequence entry : getPlaceHolders().values()) {
                    format = format.replace(key, entry);
                }
            }
            event.setFormat(MessageUtils.translate(format));
        } catch (Exception ex) {
            MessageUtils.sendToOperators("&cAn error occurred whilst setting chat format");
            MessageUtils.sendToOperators("&cCheck the console for errors.");
            ex.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String nick = string(player.getName().toLowerCase());
        if (nick != null) {
            event.setJoinMessage(event.getJoinMessage().replace(player.getName().toLowerCase(), nick));
            setNickname(player, nick);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (NicknameAPI.isNicked(player)) {
            NicknameAPI.removeNickname(player);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onTabComplete(PlayerChatTabCompleteEvent event) {
        for (String tab : event.getTabCompletions()) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (tab.equals(player.getName()) && isNicked(player)) {
                    event.getTabCompletions().remove(player.getName());
                    event.getTabCompletions().add(getNickname(player).toString());
                }
            }
        }
    }

    public static HashMap<Player, CharSequence> getNicknames() {
        return nicknames;
    }

    private String string(String string) {
        return PLUGIN.getConfig().getString(string);
    }
}

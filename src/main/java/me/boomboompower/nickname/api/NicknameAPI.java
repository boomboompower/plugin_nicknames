/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.nickname.api;

import me.boomboompower.nickname.listeners.Nicknames;

import org.apache.commons.lang.Validate;

import org.bukkit.entity.Player;

/**
 * Class to allow developers to set players nickname
 *
 * @author boomboompower
 * @version 1.0
 */
public class NicknameAPI {

    /**
     * Deny the creation of new instances
     */
    private NicknameAPI() {}

    /**
     * Returns true if is already a nickname
     *
     * @param nickname CharSequence to check if it's already a nickname
     * @return true if CharSequence is already a nickname
     */
    public static boolean isNickname(CharSequence nickname) {
        return Nicknames.getNicknames().containsValue(nickname);
    }

    /**
     * Returns true if the player is already nicked
     *
     * @param player Player to check for nickname
     * @return true if the player is nicked
     */
    public static boolean isNicked(Player player) {
        return Nicknames.getNicknames().containsKey(player) && getNickname(player) != null;
    }

    /**
     * Returns the players nickname
     *
     * @param player Gets the nickname
     * @return players nickname or {@code null} if the player doesn't have a name
     */
    public static CharSequence getNickname(Player player) {
        return Nicknames.getNicknames().get(player) != null ? Nicknames.getNicknames().get(player) : null;
    }

    /**
     * Adds a player to the nickname list
     *          invokes the {@link #isNickname(CharSequence)} and {@link #isNicked(Player)} methods
     *
     * @param player player to apply the nickname to
     * @param nickname Nickname to be added
     */
    public static void setNickname(Player player, CharSequence nickname) {
        Validate.notNull(nickname, "Nickname cannot be null!");
        player.setPlayerListName(nickname.toString());
        player.setCustomName(nickname.toString());
        if (!isNicked(player) && !isNickname(nickname)) {
            Nicknames.getNicknames().put(player, nickname);
        }
    }

    /**
     * Removes a players nickname
     *
     * @param player player to remove the nickname from
     */
    public static void removeNickname(Player player) {
        player.setPlayerListName(player.getName());
        player.setCustomName(player.getName());
        if (Nicknames.getNicknames().containsKey(player)) {
            Nicknames.getNicknames().remove(player);
        }
    }
}

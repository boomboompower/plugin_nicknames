/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.nickname.api;

import com.google.common.collect.Maps;

import java.util.HashMap;

/**
 * Class for allowing developers to add placeholders to chat format
 *
 * @author boomboompower
 * @version 1.0
 */
public class Placeholders {

    private static HashMap<CharSequence, CharSequence> placeHolders = Maps.newHashMap();

    /**
     * Deny the creation of new instances
     */
    private Placeholders() {}

    /**
     * Adds the specified charsequences to the map.
     *          invokes the {@link #isPlaceHolder(CharSequence)} method.
     *
     * @param textToReplace A charsequence you would like to be replaced. Example: %SERVERNAME%
     * @param toReplaceWith The text to replace the charsequence with. Example: "The Server Network"
     */
    public static void addPlaceholder(CharSequence textToReplace, CharSequence toReplaceWith) {
        if (!isPlaceholder(textToReplace)) placeHolders.put(textToReplace, toReplaceWith);
    }

    /**
     * Removes the specified charsequence from the map
     *
     * @param key the key to remove from the placeholders if the map contains it
     *            invokes the {@link #isPlaceholder(CharSequence)} method
     */
    public static void removePlaceholder(CharSequence key) {
        if (isPlaceholder(key)) placeHolders.remove(key);
    }

    /**
     * Returns a boolean for if the placeholder list already contains the word
     *
     * @param key charsequence to check if it's already a placeholder
     * @return true if the word is already a placeholder
     */
    public static boolean isPlaceholder(CharSequence key) {
        return placeHolders.containsKey(key);
    }

    /**
     * Gets the placeholder list.
     *
     * @return the placeholders hashmap
     */
    public static HashMap<CharSequence, CharSequence> getPlaceHolders() {
        return placeHolders;
    }
}

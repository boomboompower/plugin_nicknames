/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.nickname.api.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Sends an actionbar to any player via reflection. Shouldn't break
 *
 * @author boomboompower
 * @version 1.0
 */
public class Actionbar {

    /**
     * Deny the creation of new instances
     */
    private Actionbar() {}

    /**
     * Sends an actionbar to the specified player
     *          invokes the {@link MessageUtils#translate(String)} method
     *          invokes the {@link #getNMSClass(String)} method
     *
     * @param player Player to send actionbar to
     * @param message Message to send actionbar to (supports colors)
     */
    public static void sendActionbar(Player player, String message) {
        try {
            message = MessageUtils.translate(message);

            Object d = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + message + "\"}");
            Object e = getNMSClass("PacketPlayOutChat").getConstructor(getNMSClass("IChatBaseComponent"), byte.class).newInstance(d, (byte) 2);

            Object f = player.getClass().getMethod("getHandle").invoke(player);
            Object g = f.getClass().getField("playerConnection").get(f);

            g.getClass().getMethod("sendPacket", getNMSClass("Packet")).invoke(g, e);
        } catch (Exception e) {}
    }

    /**
     * Gets the server version package in a string format
     *
     * @return the nms version. Ex 'v1_10_R1'
     */
    private static String getVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
    }

    /**
     * Returns a NMS class for the specified string.
     *          invokes the {@link #getVersion()} method.
     *
     * <p>Example: Doing #getNMSClass("Chunk"); on 1.10 would return 'net.minecraft.server.v1_10_R1.Chunk'</p>
     *
     * @param name The NMS classname
     * @return the classname or {@code null} if it doesn't exist
     */
    private static Class<?> getNMSClass(String name) {
        try {
            return Class.forName(("net.minecraft.server." + getVersion() + "." + name));
        } catch (Exception ex) {
            return null;
        }
    }
}

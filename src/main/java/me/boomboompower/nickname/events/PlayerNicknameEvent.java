/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.nickname.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 * Allows developers to modify nicknames as they happen.
 *
 * @author boomboompower
 * @version 1.2
 */
public class PlayerNicknameEvent extends PlayerEvent implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled = false;

    private String nickname;

    /**
     * Creates a new instance of the event.
     *
     * @param player Player who is being nicked
     * @param nickname Nickname that the player is receiving
     */
    public PlayerNicknameEvent(Player player, String nickname) {
        super(player);
        this.nickname = nickname;
    }

    /**
     * Gets the specified nickname from the {@link #nickname} variable (Can be null)
     *
     * @return the nickname the player is receiving
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Sets the nickname of the player
     *
     * <p>Warning, it is not advised to give every player the same nickname</p>
     *
     * @param nickname Set the nickname from the player
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}

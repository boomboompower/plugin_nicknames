/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.nickname.api.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Class to allow formatting and editing of strings, as well as sending those strings to entities
 *
 * @author boomboompower
 * @version 1.2
 */
public class MessageUtils {

    /**
     * Deny the creation of new instances
     */
    private MessageUtils() {}

    /**
     * Colors the message. Replace & with the escape symbol
     *
     * @param message Message to translate
     * @return the chatcolor translated version of the text
     */
    public static String translate(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    /**
     * Sends a message to all online operators
     *          invokes the {@link #translate(String)} method
     *          invokes the {@link #sendToPlayer(Player, String)} method
     *
     * @param message Message to be sent to operators (supports color)
     */
    public static void sendToOperators(String message) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.isOp()) sendToPlayer(player, "&f[&cNicknames&f] " + message);
        }
    }

    /**
     * Sends a colored message/log to the console
     *          invokes the {@link #translate(String)} method
     *
     * @param message Message to be logged/sent to console
     */
    public static void sendToConsole(String message) {
        Bukkit.getConsoleSender().sendMessage(translate(message));
    }

    /**
     * Sends a message to the specified player
     *          invokes the {@link #translate(String)} method
     *
     * @param player Player which the message shall be sent to
     * @param message Message to send to the player (Supports color)
     */
    public static void sendToPlayer(Player player, String message) {
        player.sendMessage(translate(message));
    }
}
